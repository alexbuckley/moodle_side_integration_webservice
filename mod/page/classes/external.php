<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page external API
 *
 * @package    mod_page
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php"); 
require_once($CFG->dirroot.'/mod/page/lib.php');
require_once($CFG->dirroot . '/course/modlib.php');
/**
 * Page external functions
 *
 * @package    mod_page
 * @category   external
 * @copyright  2015 Juan Leyva <juan@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      Moodle 3.0
 */
class mod_page_external extends external_api {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 3.0
     */
    public static function view_page_parameters() {
        return new external_function_parameters(
            array(
                'pageid' => new external_value(PARAM_INT, 'page instance id')
            )
        );
    }

    /**
     * Simulate the page/view.php web interface page: trigger events, completion, etc...
     *
     * @param int $pageid the page instance id
     * @return array of warnings and status result
     * @since Moodle 3.0
     * @throws moodle_exception
     */
    public static function view_page($pageid) {
        global $DB, $CFG;
        require_once($CFG->dirroot . "/mod/page/lib.php");

        $params = self::validate_parameters(self::view_page_parameters(),
                                            array(
                                                'pageid' => $pageid
                                            ));
        $warnings = array();

        // Request and permission validation.
        $page = $DB->get_record('page', array('id' => $params['pageid']), '*', MUST_EXIST);
        list($course, $cm) = get_course_and_cm_from_instance($page, 'page');

        $context = context_module::instance($cm->id);
        self::validate_context($context);

        require_capability('mod/page:view', $context);

        // Call the page/lib API.
        page_view($page, $course, $cm, $context);

        $result = array();
        $result['status'] = true;
        $result['warnings'] = $warnings;
        return $result;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 3.0
     */
    public static function view_page_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_BOOL, 'status: true if success'),
                'warnings' => new external_warnings()
            )
        );
    }

 /** 
     * Describes the parameters of create_course_reading_page
     *
     * @return external_function_parameter 
     */

    public static function create_course_reading_page_parameters () {
	return new external_function_parameters (
		array(
			'courseitem' => new external_multiple_structure (
				new external_single_structure (
		  			array (
						'courseid'=> new external_value(PARAM_INT, 'id of the course'),
		  				'title' => new external_value(PARAM_TEXT, 'title of item'),
		  				'frameworkcode' => new external_value(PARAM_TEXT, 'item framework code', VALUE_OPTIONAL),
		  				'author' => new external_value(PARAM_TEXT, 'item author(s)', VALUE_OPTIONAL),
		  				'unititle' => new external_value(PARAM_TEXT, 'item unititle', VALUE_OPTIONAL),
		  				'notes' => new external_value(PARAM_TEXT, 'item notes', VALUE_OPTIONAL),
		  				'serial' => new external_value(PARAM_TEXT, 'item serial number', VALUE_OPTIONAL),
		  				'seriestitle' => new external_value(PARAM_TEXT, 'item series title', VALUE_OPTIONAL),
		  				'copyrightdate' => new external_value(PARAM_TEXT, 'item copyright date', VALUE_OPTIONAL),
		  				'timestamp' => new external_value(PARAM_TEXT, 'item timestamp', VALUE_OPTIONAL),
		  				'datecreated' => new external_value(PARAM_TEXT, 'item creation date', VALUE_OPTIONAL),
		  				'abstract' => new external_value(PARAM_TEXT, 'item abstract', VALUE_OPTIONAL),
	          				'itype' => new external_value(PARAM_TEXT, 'item type of item', VALUE_OPTIONAL),
		  				'volume' => new external_value(PARAM_TEXT, 'Volume of item', VALUE_OPTIONAL),
		  				'number' => new external_value(PARAM_TEXT, 'Issue number of item', VALUE_OPTIONAL),
		  				'publishyear' => new external_value(PARAM_TEXT, 'Items publication year', VALUE_OPTIONAL),
		  				'isbn' => new external_value(PARAM_TEXT, 'ISBN number of item', VALUE_OPTIONAL),
					)
				)
			)
		)	
	);
    }   

	/*
	* Creates a single reading page
	*	
	* @param int $courseid. ID of the course
	* @param string $title. Title of item
	* @param string $frameworkcode. Item framework
	* @param string $author. Item author
	* @param string $unititle
	* @param string $notes
	* @param string $serial 
	* @param string $seriestitle
	* @param string $copyrightdate
	* @param string $timestamp
	* @param string $datecreated
	* @param string $abstract
	* @param string $itype
	* @param string $volume
	* @param string $number
	* @param string $publishyear
	* @param string $isbn
	* @return array of warnings and page data.
	**/

   public static function create_course_reading_page ($courseitem) {
        global $USER, $DB;

	$params = self::validate_parameters(self::create_course_reading_page_parameters(), 
		array ('courseitem' => $courseitem));
	$page_info = array();
	

	//Create the page 
    	$data = new stdClass();
    	$data->course = $courseitem[0]['courseid'];
    	$data->name = $courseitem[0]['title'];
    	$data->intro = '<p>'.$courseitem[0]['title'].'</p>';
    	$data->introformat = FORMAT_HTML;
    	if ($uploadinfo->type == 'text/html') {
      	  $data->contentformat = FORMAT_HTML;
       	  $data->content = clean_param($courseitem[0]['notes'], PARAM_CLEANHTML);
    	} else {
       	  $data->contentformat = FORMAT_PLAIN;
          $data->content = clean_param($courseitem[0]['notes'], PARAM_TEXT);
    	}
	//$data->coursemodule = 2;

// Set the display options to the site defaults.
    	$config = get_config('page');
    	$data->display = $config->display;
    	$data->popupheight = $config->popupheight;
    	$data->popupwidth = $config->popupwidth;
    	$data->printheading = $config->printheading;
    	$data->printintro = $config->printintro;
    	$new_page = page_add_instance($data, null); //Create new page
	$result = array();
        $result['id'] = $new_page;

//At present this page is not associated with any course module so we'll create one now

	$coursem = new stdClass();
	$coursem->course = $data->course;
	$coursem->module = '15';
	$coursem->instance = $new_page;
	$coursem->section = 0;
	$sectionid = 0;

	if (! $coursem->coursemodule = add_course_module($coursem) ) {   // assumes course/lib.php is loaded
  	  echo $OUTPUT->notification("Could not add a new course module to the course '" . $courseid . "'");
   	 return false;
	}

	if (! $sectionid = course_add_cm_to_section($data->course, $coursem->coursemodule, $sectionid) ) {   // assumes course/lib.php is loaded
		echo $OUTPUT->notification("Could not add the new course module to that section");
		return false;
	}
	//$DB->set_field("course_modules", "section", $sectionid, array("id" => $coursem->coursemodule));

	rebuild_course_cache($data->course,true);
        return $result;
   }


/* Define return parameters function */
public static function create_course_reading_page_returns () {
 return new external_single_structure ( //This is used to represent a single activity.
   array(
	'id'=> new external_value(PARAM_INT, 'page id'),
   )
 );
}


}
